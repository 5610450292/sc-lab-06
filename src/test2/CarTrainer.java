package test2;

public class CarTrainer {
	
	public CarTrainer() {	
	}
	
	public String makeCarMove (Truck truck) {
		return "Truck " + truck.Move() ;
	}
	
	public String makeCarMove (Mortercycle honda) {
		return "Mortercycle " + honda.Move() ;
	}
	
	public String makeCarMove (Car car) {
		return "Car " + car.Move() ;
	}

}
