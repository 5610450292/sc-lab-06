package test2;

public abstract class Car {
	
	private String name ;
	
	public Car(String name) {  // default constructor
		this.name = name ;
	}
	
	
	public abstract String Move () ;
	
	public String toString(){
		return this.name;
	}

}
