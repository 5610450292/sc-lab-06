package test2;

import java.util.ArrayList;

public class Main {

	public static void main(String[] args) {
		Truck Truck = new Truck("MM name");
		Mortercycle Mortercycle = new Mortercycle("NN name");
		
		System.out.println(Truck);
		System.out.println(Mortercycle);
		
		ArrayList<Car> car = new ArrayList<Car>();
		car.add(Truck);
		car.add(Mortercycle);
		
		for (Car cars: car){
			System.out.println(cars.Move());
		}
		
		CarTrainer carTrain = new CarTrainer();
		System.out.println(carTrain.makeCarMove(Truck));
		System.out.println(carTrain.makeCarMove(Mortercycle));
		System.out.println(carTrain.makeCarMove((Car) Mortercycle));		
	}

}
