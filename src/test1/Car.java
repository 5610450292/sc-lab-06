package test1;

public abstract class Car {
	private String name ;
	
	public Car(String name) {  // default constructor
		this.name = name ;
	}
	
	
	public abstract String Brand () ;
	
	public String toString(){
		return this.name;
	}

}
